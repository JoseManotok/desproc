using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WordManager : MonoBehaviour
{
    public static WordManager Instance;

    [SerializeField]
    TextAsset wordList;

    public List<WordStruct> Easy = new List<WordStruct>();
    public List<WordStruct> Average = new List<WordStruct>();
    public List<WordStruct> Hard = new List<WordStruct>();

    public List<string> EasyWords;
    public List<string> AveWords;
    public List<string> HardWords;

    public List<GameObject> Words;
    
    public int WordNumber;
    //0 = easy
    //1 = ave
    //2 = hard

    public int GameDifficulty = 1;
    
    void Awake()
    {
        InitWordManager();
        //CreateWords();
    }

    void CreateWords()
    {
        int wordsIndex = 0;
        foreach (string w in wordList.text.Split(new char[] { '\n' }))
        {
            string getWord = w.Trim();
            if(getWord == "=")
            {
                wordsIndex++;
                continue;
            }

            switch (wordsIndex)
            {
                case 0:
                    EasyWords.Add(w);
                    Easy.Add(new WordStruct(w, Resources.Load<AudioClip>("Audio/" + getWord.ToUpper())));
                    break;
                case 1:
                    AveWords.Add(w);
                    Average.Add(new WordStruct(w, Resources.Load<AudioClip>("Audio/" + getWord.ToUpper())));
                    break;
                case 2:
                    HardWords.Add(w);
                    Hard.Add(new WordStruct(w, Resources.Load<AudioClip>("Audio/" + getWord.ToUpper())));
                    break;
                     

            }
        }
    }

    // Start is called before the first frame update
    void Start()
    {
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void RemoveWordFromList(int WordNumber)
    {
        switch (GameDifficulty)
        {
            case 0:
                EasyWords.Remove(EasyWords[WordNumber]);
                break;
            case 1:
                AveWords.Remove(AveWords[WordNumber]);
                break;
            case 2:
                HardWords.Remove(HardWords[WordNumber]);
                break;
        }
    }

    public void SetDifficulty(int DifNumber)
    {
        GameDifficulty = DifNumber;
    }

    void InitWordManager()
    {
        if (Instance == null)
        {
            DontDestroyOnLoad(gameObject);
            Instance = this;
        }
        else
        {
            if (Instance != this)
            {
                Destroy(gameObject);
            }
        }
    }

}

[System.Serializable]
public struct WordStruct
{
    public string Word;
    public AudioClip clip;

    public WordStruct(string mWord, AudioClip mClip)
    {
        Word = mWord;
        clip = mClip;
    }

}