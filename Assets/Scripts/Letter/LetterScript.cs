using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Linq;
using System;

public class LetterScript : MonoBehaviour, InterfacePooledObject
{
    Rigidbody2D RB2D;
    MainGameScript MainScript;
    WordManager WManager;
    string LetterText;

    Animator LetterAnimation;

    public GameObject childAnim;
    public int LetterSpeed = 1;
    public float LetterSpeedModifier;
    public bool WasInteracted;
    public bool WasShot = false;

    public List<GameObject> MatchingLetters;

    // Start is called before the first frame update
    public void OnObjectSpawn()
    {
        gameObject.name = "Letter";

        MainScript = MainGameScript.GetInstance;
        WManager = WordManager.Instance;
        RB2D = GetComponent<Rigidbody2D>();
        LetterText = GetComponent<TextMesh>().text;
        LetterAnimation = childAnim.GetComponent<Animator>();

        MatchingLetters.Clear();

        GameEvents.current.OnPlayerLose += DisableLetterMovement;
    }

    public void Start()
    {
        switch (WManager.GameDifficulty)
        {
            case 0:
                LetterSpeedModifier = 0.15f;
                break;

            case 1:
                LetterSpeedModifier = 0.25f;
                break;

            case 2:
                LetterSpeedModifier = 0.35f;
                break;
            default:
                break;
        }
    }

    // Update is called once per frame
    void Update()
    {
        PreviewLetter();
        BulletLetter();
        BubbleLetter();
        FixList();

        if (MatchingLetters.Count > 0)
        {
            for (int i = 0; i < MatchingLetters.Count; i++)
            {
                if (!MatchingLetters[i].activeInHierarchy)
                {
                    PopThis();
                   
                }
            }
        }
    }

    private void FixList()
    {
        MatchingLetters.Distinct().ToList();
    }

    private void PreviewLetter()
    {
        if (gameObject.name == "Preview")
        {
            gameObject.GetComponent<CircleCollider2D>().enabled = false;
        }
        else
        {
            gameObject.GetComponent<CircleCollider2D>().enabled = true;
        }
    }

    private void BulletLetter()
    {
        if (gameObject.name == "Bullet")
        {
            transform.Translate(Vector2.up * LetterSpeed * Time.deltaTime);
            WasShot = true;
        }
    }

    private void BubbleLetter()
    {
        if (gameObject.name == "Letter")
        {
            RB2D.velocity = new Vector2(0f, 0f);
            transform.Translate(Vector2.up * (-LetterSpeed * (LetterSpeedModifier * 0.025f)) * Time.deltaTime);
            transform.rotation = Quaternion.identity;
        }

        if (gameObject.transform.position.y < -10)
        {
            gameObject.SetActive(false);
        }
    }

    public void PopThis()
    {
        //LetterAnimation.SetTrigger("Pop");
        //gameObject.SetActive(false);
      
        StartCoroutine(PopThisBubble());
    }

    public void PopBubble(GameObject PoppedBubble)
    {
       
            
        Debug.Log("Popping Bubble");
        PoppedBubble.SetActive(false);
        LetterAnimation.SetTrigger("Pop");
        gameObject.SetActive(false);
    }

    private void OnCollisionEnter2D(Collision2D collision)
    {
        //bullet
        if (this.gameObject.name == "Bullet")
        {
            if (collision.collider.name == "Letter" || collision.collider.name == "Border")
            {
                if (collision.collider.tag == this.gameObject.tag)
                {
                    Debug.Log("Match hit");
                    LetterScript bubble = collision.collider.gameObject.GetComponent<LetterScript>();
                    bubble.PopThis();
                    PopThis();
                     //PopBubble(collision.collider.gameObject);
                    //StartCoroutine(popBulletBubble(collision.collider.gameObject));
                    MainScript.CurrentWord += LetterText;
                }
                gameObject.name = "Letter";
            }
        }

        //letter
        if (this.gameObject.name == "Letter")
        {
            if (collision.collider.CompareTag(this.gameObject.tag))
            {
                MatchingLetters.Add(collision.gameObject);
            }
        }
    }

    private void OnCollisionExit2D(Collision2D collision)
    {

    }
    
    private void DisableLetterMovement()
    {
        LetterSpeed = 0;
    }

    IEnumerator PopThisBubble()
    {
      
        LetterAnimation.SetTrigger("Pop");
        yield return new WaitForSeconds(0.9f);
        
        gameObject.SetActive(false);
    }

    //IEnumerator popBulletBubble(GameObject pop)
    //{
    //    Animator anim = pop.GetComponentInChildren<Animator>();

    //    anim.SetTrigger("Pop");
    //    yield return new WaitForSeconds(0.9f);
    //    pop.SetActive(false);
    //}
}

